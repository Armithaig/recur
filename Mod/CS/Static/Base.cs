using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using QupKit;
using XRL;
using XRL.UI;
using XRL.World;
using XRL.World.Anatomy;
using XRL.World.Capabilities;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Parts.Mutation;

namespace Recur
{
	public static partial class Static
	{
		public static void RLog(string message, Exception e = null)
		{
			message = "RECUR - " + message;
			if (e != null) UnityEngine.Debug.LogError(message + "\n" + e.ToString());
			else UnityEngine.Debug.Log(message);
		}

		public static string MakeObjectID(GameObject obj)
		{
			var display = obj.DisplayNameOnlyDirectAndStripped;
			var result = Regex.Replace(display, @"\W", "");
			if (String.IsNullOrEmpty(result) || result == obj.Blueprint)
				result = obj.id;
			return result;
		}

		public static bool TryParseDate(string Value, string Format, out DateTime Date)
		{
			return DateTime.TryParseExact(Value, Format, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out Date);
		}

		public static string JoinObjectionary(Object obj, string divider = ":", string separator = ";")
		{
			if (obj == null) return String.Empty;

			var dict = StringifyObjectionary(obj);
			if (dict.Count <= 0) return String.Empty;

			var sb = new StringBuilder();
			foreach (var pair in dict)
			{
				try
				{
					if (sb.Length > 0) sb.Append(separator);

					sb.Append(pair.Key).Append(divider).Append(pair.Value);
				}
				catch (Exception e)
				{
					RLog("Error joining objectionary pair " + pair, e);
				}
			}

			return sb.ToString();
		}

		public static Dictionary<string, string> StringifyObjectionary(object obj)
		{
			var result = new Dictionary<string, string>();
			if (obj == null) return result;

			var dict = obj as IDictionary;
			if (dict.Count <= 0) return result;

			foreach (var pair in dict)
			{
				try
				{
					var type = pair.GetType();
					var key = type.GetProperty("Key").GetValue(pair, null).ToString();
					var value = type.GetProperty("Value").GetValue(pair, null).ToString();

					result[key] = value;
				}
				catch (Exception e)
				{
					RLog("Error stringifying objectionary pair " + pair, e);
				}
			}

			return result;
		}

		public static void ClearItems(GameObject obj)
		{
			var inventory = obj.GetPart("Inventory") as Inventory;
			if (inventory != null) inventory.Clear();

			var body = obj.GetPart("Body") as Body;
			if (body != null && body.GetBody() != null)
			{
				var unequip = Event.New("CommandUnequipObject", "IsSilent", 1, "SemiForced", 1, "NoTake", 1);

				Action<BodyPart> action = null;
				action = x =>
				{
					try
					{
						if (x.Equipped != null)
						{
							unequip.SetParameter("Object", x.Equipped);
							unequip.SetParameter("BodyPart", x);
							body.FireEvent(unequip);
						}

						x.Unimplant(false);
					}
					catch (Exception e)
					{
						RLog("Error unequipping from " + x.Name, e);
					}

					x.Parts?.ForEach(action);
				};
				action(body.GetBody());
			}
		}

		// Get true base values without mutations or implants.
		// Deep copying and removing/unmutating led to nasty side-effects.
		public static Dictionary<string, int> GetBaseValueDiff(GameObject obj)
		{
			var scratch = GameObjectFactory.Factory.CreateObject(obj.Blueprint);
			scratch.Statistics["Level"] = obj.Statistics["Level"]; // Level's used as a parameter in Leveler:StatChange_Hitpoints/Intelligence.
			var baseValues = new Dictionary<string, int>();
			var diffs = new Dictionary<string, int>();

			Action rebase = () =>
			{
				scratch.Statistics.Values.All(x =>
				{
					baseValues[x.Name] = x.BaseValue;
					return true;
				});
			};

			Action rediff = () =>
			{
				scratch.Statistics.Values.All(x =>
				{
					if (!diffs.ContainsKey(x.Name)) diffs.Add(x.Name, 0);
					diffs[x.Name] += x.BaseValue - baseValues[x.Name];
					return true;
				});
			};

			var mutations = scratch.RequirePart<Mutations>();
			obj.GetPartsDescendedFrom<BaseMutation>()
				.All(x =>
				{
					try
					{
						rebase();
						var mutation = scratch.GetPart(x.Name) as BaseMutation;
						if (mutation == null)
						{
							var type = ModManager.ResolveType("XRL.World.Parts.Mutation." + x.Name);
							mutation = Activator.CreateInstance(type) as BaseMutation;
							mutations.AddMutation(mutation, 1);
						}

						mutation.BaseLevel = x.Level;
						mutation.ChangeLevel(mutation.Level);
						rediff();
					}
					catch (Exception e)
					{
						RLog("Error evaluating mutation diff " + x.Name, e);
					}

					return true;
				});

			var skills = scratch.RequirePart<Skills>();
			obj.GetPartsDescendedFrom<BaseSkill>()
				.All(x =>
				{
					try
					{
						if (scratch.HasPart(x.Name)) return true;
						rebase();
						var type = ModManager.ResolveType("XRL.World.Parts.Skill." + x.Name);
						var skill = Activator.CreateInstance(type) as BaseSkill;
						skills.AddSkill(skill);
						rediff();
					}
					catch (Exception e)
					{
						RLog("Error evaluating skill diff " + x.Name, e);
					}

					return true;
				});

			var scrbod = scratch.RequirePart<Body>();
			var body = obj.GetPart<Body>();
			if (body != null && body.GetBody() != null)
			{
				var equip = Event.New("CommandEquipObject", "IsSilent", 1, "SemiForced", 1);

				Action<BodyPart> action = null;
				action = x =>
				{
					var y = scrbod.GetPartByDescription(x.Description);
					if (y == null) return;

					if (x.Equipped != null)
					{
						try
						{
							rebase();
							var item = GameObjectFactory.Factory.CreateObject(x.Equipped.Blueprint);
							equip.SetParameter("Object", item);
							equip.SetParameter("BodyPart", y);
							scratch.FireEvent(equip);
							rediff();
						}
						catch (Exception e)
						{
							RLog("Error evaluating equipment diff " + x.Name, e);
						}
					}

					if (x.Cybernetics != null)
					{
						try
						{
							rebase();
							var item = GameObjectFactory.Factory.CreateObject(x.Cybernetics.Blueprint);
							y.Implant(item);
							rediff();
						}
						catch (Exception e)
						{
							RLog("Error evaluating implant diff " + x.Name, e);
						}
					}

					x.Parts?.ForEach(action);
				};
				action(body.GetBody());
			}

			return diffs;
		}

		/// <summary>
		/// Correct body parts orphaned in a laterality axis.
		/// </summary>
		public static void CorrectLaterality(BodyPart parent)
		{
			if (parent.Parts == null) return;

			foreach (var part in parent.Parts)
			{
				if (part.Laterality == Laterality.NONE) continue;
				var newLat = part.Laterality;

				foreach (var pair in Laterality.Axes)
				{
					var pos = newLat & pair.Value[0];
					if (pos == 0) continue;
					if (part.HasAxisSibling(parent.Parts, newLat, pair.Value[0], pos)) continue;

					newLat -= pos;
					// RLog($"Removed {Laterality.GetAxisName(pair.Key)} axis from {part.Description}, now {Laterality.WithLateralityAdjective(part.VariantType, newLat)}");
				}

				// Assumes children inherit laterality which'd be untrue of more chimeric bodies.
				// TODO: Do... something, ugh!
				part.ChangeLaterality(newLat, true);
			}
		}

		// TODO: Handle dismembered siblings, accounting for their nulled sub-parts.
		// Using XMLBodyPart directly would be easier for this.
		// Alternatively dismember after laterality correction.
		// Assuming each axis must have a sibling is also dangerous, perhaps only worry about lateral.
		public static bool HasAxisSibling(this BodyPart part, IEnumerable<BodyPart> siblings, int lat, int axis, int pos)
		{
			foreach (var sib in siblings)
			{
				if (part == sib) continue;
				if (part.Type != sib.Type) continue;

				var sibPos = sib.Laterality & axis;
				if (sibPos == 0) continue;
				if ((lat - pos) == (sib.Laterality - sibPos))
				{
					// RLog($"{part.Description} found {Laterality.LateralityAdjective(sibPos)} sibling");
					return true;
				}
			}

			return false;
		}

		/*public static void Traverse(UnityEngine.GameObject obj, string fileName) {
			var writer = new StreamWriter(fileName, false);
			Traverse(writer, obj);
			writer.Flush();
			writer.Close();
			writer.Dispose();
		}

		public static void Traverse(StreamWriter writer, UnityEngine.GameObject obj, int depth = 0) {
			var indent = new String(' ', depth * 4);
			var memberIndent = new String(' ', (depth + 1) * 4);
			writer.WriteLine(indent + "=============");
			writer.WriteLine(indent + "= " + obj + " =");
			try {
				object value = null;
				foreach (var member in obj.GetType().GetMembers()) {
					if (member.MemberType == MemberTypes.Field)
						value = ((FieldInfo)member).GetValue(obj);
					else if (member.MemberType == MemberTypes.Property)
						value = ((PropertyInfo)member).GetValue(obj, null);
					if (value != null && value.GetType() != typeof(UnityEngine.Matrix4x4))
						writer.WriteLine(memberIndent + member.Name + ": " + value);
				}
			} catch (Exception e) {
				RLog("Error traversing object " + obj.name, e);
			}
			foreach (var comp in obj.GetComponents<UnityEngine.Component>()) {
				writer.WriteLine(indent + "= " + comp.GetType() + " =");
				try {
					object value = null;
					foreach (var member in comp.GetType().GetMembers()) {
						if (member.MemberType == MemberTypes.Field)
							value = ((FieldInfo)member).GetValue(comp);
						else if (member.MemberType == MemberTypes.Property)
							value = ((PropertyInfo)member).GetValue(comp, null);
						if (value != null && value.GetType() != typeof(UnityEngine.Matrix4x4))
							writer.WriteLine(memberIndent + member.Name + ": " + value);
					}
				} catch (Exception e) {
					RLog("Error traversing component " + comp.name, e);
				}
			}
			foreach (UnityEngine.Transform transform in obj.transform) {
				var child = transform.gameObject;
				Traverse(writer, child, depth + 1);
			}
		}*/
	}
}
