using System;
using System.Xml;
using System.Linq;
using System.Text.RegularExpressions;

using XRL;
using XRL.World;
using XRL.World.Anatomy;
using XRL.World.Parts;
using XRL.World.Parts.Mutation;

using static Recur.Static;

namespace Recur
{
	public static class Extensions
	{
		/// <summary>
		/// Write element with values as attributes.
		/// </summary>
		/// <param name="localName">Element name</param>
		/// <param name="clear">Skip writing empty values</param>
		/// <param name="attributes">Alternating keys and values</param>
		public static void WriteElementString(this XmlWriter writer, string localName, bool clear, params string[] attributes) {
			if (attributes.Length < 2) return;

			writer.WriteStartElement(localName);
			for (int i = 0; i < attributes.Length; i += 2) {
				var key = attributes[i];
				var value = attributes[i + 1];

				if (String.IsNullOrEmpty(key)) continue;
				if (clear && String.IsNullOrEmpty(value)) continue;

				writer.WriteAttributeString(key, value);
			}
			writer.WriteEndElement();
		}

		/// <summary>
		/// Return body part that this implant is inside.
		/// </summary>
		public static BodyPart ImplantedOn(this GameObject implant) {
			var obj = implant.GetPart<CyberneticsBaseItem>()?.ImplantedOn;
			if (obj != null) {
				var body = obj.GetPart("Body") as Body;
				if (body != null) return body.FindCybernetics(implant);
			}
			return null;
		}

		/// <summary>
		/// Return mutation entry by display name rather than class.
		/// Starts with a dictionary lookup before comparing through regex.
		/// </summary>
		public static MutationEntry GetEntryByName(this BaseMutation mutation) {
			var result = MutationFactory.GetMutationEntryByName(mutation.DisplayName);
			if (result != null) return result;

			var mutations = MutationFactory.AllMutationEntries();
			var regex = new Regex(@"[^\w\d\s]");
			var name = regex.Replace(mutation.DisplayName.ToLower(), "");
			return mutations.Where(x => regex.Replace(x.DisplayName.ToLower(), "") == name).FirstOrDefault();
		}

		/// <summary>
		/// Add object to random empty cell.
		/// </summary>
		public static Cell GetRandomEmptyLocalAdjacentCell(this Cell C, int Radius = 1) {
			var cells = C.GetLocalAdjacentCells(Radius);
			if (cells == null || cells.Count == 0) return C;

			return cells.GetRandomElement(RANDOM);
		}

		public static char GetMainColor(this Render Render)
		{
			if (!Render.TileColor.IsNullOrEmpty())
			{
				var i = Render.TileColor.LastIndexOf('&');
				if (i >= 0) return Render.TileColor[i + 1];
			}

			if (!Render.ColorString.IsNullOrEmpty())
			{
				var i = Render.ColorString.LastIndexOf('&');
				if (i >= 0) return Render.ColorString[i + 1];
			}

			return 'y';
		}
	}
}
