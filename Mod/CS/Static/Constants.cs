using System;
using System.IO;

using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;

using static XRL.UI.Options;

namespace Recur
{
	public static partial class Static
	{
		// Properties
		public static DirectoryInfo CharDir => new DirectoryInfo(DataManager.SavePath("Characters"));
		// Options
		public static bool ImportFollower => GetOption("OptionRecurFollower", "Yes").EqualsNoCase("Yes");
		public static bool ImportBody => GetOption("OptionRecurBody", "Yes").EqualsNoCase("Yes");
		public static bool ImportReputation => GetOption("OptionRecurReputation", "Yes").EqualsNoCase("Yes");
		public static bool ImportTinker => GetOption("OptionRecurTinker", "Yes").EqualsNoCase("Yes");
		public static bool ImportJournal => GetOption("OptionRecurJournal", "Yes").EqualsNoCase("Yes");
		// public static bool ExecuteMutator => GetOption("OptionRecurMutator", "No").EqualsNoCase("Yes");
		// Events
		public static readonly Event eCommandEquipObject = new Event("CommandEquipObject", "IsSilent", 1, "SemiForced", 1, "EnergyCost", 0);
		public static readonly Event eCommandTakeObject = new Event("CommandTakeObject", "IsSilent", 1, "EnergyCost", 0);
		// Random
		[GameBasedStaticCache(CreateInstance = false)]
		static Random random;
		public static Random RANDOM {
			get {
				if (random == null)
					random = Stat.GetSeededRandomGenerator("Recur");
				return random;
			}
		}
	}
}
