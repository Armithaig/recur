using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Recur.XML;
using UnityEngine;
using static Recur.Static;
using Calendar = XRL.World.Calendar;
using ColorUtility = ConsoleLib.Console.ColorUtility;
using GameObject = XRL.World.GameObject;

namespace Recur
{
    public class Aspect : IComparable<Aspect>
    {
        public string ID { get; set; }
        public FileInfo File { get; set; }
        public Dictionary<string, string> Meta { get; set; }
        public DateTime Date { get; set; }

        public XMLGameObject Player { get; set; }
        public XMLRender Render { get; set; }
        public List<XMLGameObject> Companions { get; set; }

        public XMLReputation Reputation { get; set; }
        public XMLTinker Tinker { get; set; }
        public XMLJournal Journal { get; set; }

        public Aspect()
        {
            Meta = new Dictionary<string, string>();
            Companions = new List<XMLGameObject>();
        }

        public Aspect(FileInfo file) : this()
        {
            File = file;
            Date = file.CreationTime;
            ID = file.Name;
        }

        public Aspect(string path) : this(new FileInfo(path))
        {
        }

        public Aspect(FileInfo file, Dictionary<string, string> meta) : this(file)
        {
            Meta = meta;
            if (meta.TryGetValue("Date", out var str) && TryParseDate(str, "yyyyMMddHHmmss", out var date))
            {
                Date = date;
            }
        }

        public string GetTitle()
        {
            if (Meta.TryGetValue("Name", out var result)) return result;
            return "Unknown";
        }

        public string GetDescription()
        {
            var sb = new StringBuilder();
            if (Meta.TryGetValue("Level", out var str))
            {
                sb.Append("{{c|\u00F9}} ").Append("Level ").Append(str);
            }

            if (Meta.TryGetValue("Genotype", out str))
            {
                if (sb.Length == 0) sb.Compound("{{c|\u00F9}} ", '\n');
                else sb.Compound("{{K|\u0010}} ", ' ');
                sb.Append(str);
                if (Meta.TryGetValue("Subtype", out str))
                {
                    sb.Compound(str, ' ');
                }
            }
            else if (Meta.TryGetValue("Subtype", out str) || Meta.TryGetValue("Type", out str))
            {
                if (sb.Length == 0) sb.Compound("{{c|\u00F9}} ", '\n');
                else sb.Compound("{{K|\u0010}} ", ' ');
                sb.Append(str);
            }

            if (Meta.TryGetValue("Time", out str) && Int64.TryParse(str, out var time))
            {
                sb.Compound("{{c|\u00F9}} Departed on the ", '\n')
                    .Append(Calendar.GetDay(time))
                    .Append(" of ").Append(Calendar.GetMonth(time));
            }

            if (Meta.TryGetValue("Companions", out str) && !str.IsNullOrEmpty())
            {
                var epithets = str.IndexOf(',') >= 0;
                var companions = str.Split(';');
                sb.Compound("{{c|\u00F9}} Followed by ", '\n');
                for (int i = 0, c = companions.Length; i < c; i++)
                {
                    if (i > 0) sb.Append(epithets ? "; " : ", ");
                    if (i == c - 1 && i > 0) sb.Append("and ");
                    sb.Append(companions[i]);
                }
            }

            if (Date != DateTime.MinValue)
            {
                sb.Compound("{{K|\u0007 ", "\n\n").Append(Date.ToString("f")).Append("}}");
            }

            return sb.ToString();
        }

        public string GetTile()
        {
            if (Render != null)
            {
                return Render.Tile;
            }

            if (Meta.TryGetValue("Tile", out var str))
            {
                return str;
            }

            return "Mutations/temporal_fugue.bmp";
        }

        public Color GetDetail()
        {
            if (Render != null)
            {
                return ColorUtility.ColorMap[Render.DetailColor[0]];
            }

            if (Meta.TryGetValue("Detail", out var str) && !str.IsNullOrEmpty())
            {
                return ColorUtility.ColorMap[str[0]];
            }

            return ColorUtility.ColorMap['K'];
        }

        public Color GetForeground()
        {
            var fg = Render?.GetForeground();
            if (fg != null)
            {
                return ColorUtility.ColorMap[fg.Value];
            }

            if (Meta.TryGetValue("Foreground", out var str) && !str.IsNullOrEmpty())
            {
                return ColorUtility.ColorMap[str[0]];
            }

            return ColorUtility.ColorMap['y'];
        }

        public string GetPlayer()
        {
            var sb = new StringBuilder();
            string buf;
            if (Meta.TryGetValue("Name", out buf))
            {
                sb.Append(buf);
                if (Meta.TryGetValue("Level", out buf))
                    sb.AppendFormat(", Level {0}", buf);
                if (Meta.TryGetValue("Genotype", out buf))
                    sb.AppendFormat(" {0}", buf);
                if (Meta.TryGetValue("Subtype", out buf))
                    sb.AppendFormat(" {0}", buf);
            }

            return sb.ToString();
        }

        public string GetCompanions()
        {
            if (Meta.TryGetValue("Companions", out var str) && !String.IsNullOrEmpty(str))
            {
                return String.Join(", ", str.Split(';'));
            }

            return "None";
        }

        public string GetDate()
        {
            if (Meta.TryGetValue("Date", out var str))
            {
                DateTime date;
                if (DateTime.TryParseExact(str, "yyyyMMddHHmmss", CultureInfo.InvariantCulture,
                    DateTimeStyles.AllowWhiteSpaces, out date))
                    return date.ToString("f");
            }

            return String.Empty;
        }

        public string GetTime()
        {
            if (Meta.TryGetValue("Time", out var str) && Int64.TryParse(str, out var time))
            {
                int day = (int) time % 438000;
                return $"{Calendar.getDay(day)} of {Calendar.getMonth(day)}";
            }

            return String.Empty;
        }

        public int CompareTo(Aspect Other)
        {
            return Other.Date.CompareTo(Date);
        }

        public override string ToString()
        {
            return GetPlayer();
        }
    }
}
