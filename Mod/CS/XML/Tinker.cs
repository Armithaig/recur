using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using XRL;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Tinkering;

using static Recur.Static;

namespace Recur.XML
{

	[XmlType("Tinker")]
	public class XMLTinker
	{
		[XmlElement("Bit")]
		public List<XMLPair> Bits { get; set; } = new List<XMLPair>();

		[XmlElement("Blueprint")]
		public List<XMLPair> Blueprints { get; set; } = new List<XMLPair>();

		[XmlElement("Mod")]
		public List<XMLPair> Mods { get; set; } = new List<XMLPair>();

		[XmlElement("Examined")]
		public List<XMLPair> Examined { get; set; } = new List<XMLPair>();

		public XMLTinker() { }
		public XMLTinker(bool auto) {
			var player = The.Player;
			var locker = player?.GetPart<BitLocker>();
			if (locker != null) {
				locker.BitStorage.OrderBy(x => x.Key)
					.All(x => {
						Bits.Add(new XMLPair(x.Key.ToString(), x.Value.ToString()));
						return true;
					});
			}

			TinkerData.KnownRecipes.OrderBy(x => x.PartName)
				.All(x => {
					var entry = new XMLPair(x.PartName);
					if (x.Type == "Mod") Mods.Add(entry);
					else Blueprints.Add(entry);
					return true;
				});

			Examiner.UnderstandingTable.OrderBy(x => x.Key)
				.All(x => {
					Examined.Add(new XMLPair(x.Key, x.Value.ToString()));
					return true;
				});
		}

		public void Apply() {
			var player = The.Player;
			var locker = player?.GetPart<BitLocker>();
			if (locker != null) {
				Bits.All(x => {
					int value;
					if (Int32.TryParse(x.Value, out value))
						locker.BitStorage[x.Name[0]] = value;

					return true;
				});
			}

			Examined.All(x => {
				int value;
				if (Int32.TryParse(x.Value, out value))
					Examiner.UnderstandingTable[x.Name] = value;

				return true;
			});

			Blueprints.All(x => {
				GameObjectFactory.Factory.CreateSampleObject(x.Name)?.Destroy(); // Create sample so cost's added to BitCostMap.
				TinkerData.LearnBlueprint(x.Name);
				return true;
			});

			Mods.All(x => {
				TinkerData.LearnMod(x.Name);
				return true;
			});

		}
	}
}
