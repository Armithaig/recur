using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

using XRL;
using XRL.Core;
using XRL.Language;
using XRL.World;

using static Recur.Static;

namespace Recur.XML
{
	public static class Exporter
	{
		public static bool Export() {
			var dir = CharDir;
			dir.Create();
			dir.Refresh();

			var player = The.Player;
			var companions = player.CurrentCell.ParentZone.GetObjects(x => x.IsPlayerLed());

			var srl = XmlSerializer.FromTypes(new[] { typeof(XMLGameObject), typeof(XMLReputation), typeof(XMLTinker), typeof(XMLJournal) });
			var ns = new XmlSerializerNamespaces(new[] { new XmlQualifiedName(string.Empty, "https://amaranth.red") });
			var file = Path.Combine(dir.FullName, String.Format("{0}.{1:yyyyMMddHHmmss}.xml", MakeObjectID(player), DateTime.Now));
			var writer = XmlWriter.Create(
				file,
				new XmlWriterSettings {
					Indent = true,
					CloseOutput = true,
					CheckCharacters = false,
					IndentChars = "\t",
					NewLineChars = Environment.NewLine,
					OmitXmlDeclaration = true,
					ConformanceLevel = ConformanceLevel.Fragment
				});

			try {
				WriteMetaData(writer, player, companions);

				writer.WriteStartElement("GameObjects");
				srl[0].Serialize(writer, new XMLGameObject(player, true), ns);
				companions.All(x => {
					srl[0].Serialize(writer, new XMLGameObject(x, true), ns);
					return true;
				});
				writer.WriteFullEndElement();

				srl[1].Serialize(writer, new XMLReputation(true), ns);
				srl[2].Serialize(writer, new XMLTinker(true), ns);
				srl[3].Serialize(writer, new XMLJournal(true), ns);
				writer.Close();
				return true;
			} catch (Exception e) {
				RLog("Error exporting character", e);
				try { File.Delete(file); } catch (Exception) { }
			}
			return false;
		}

		public static void WriteMetaData(XmlWriter writer, GameObject player, IEnumerable<GameObject> companions)
		{
			try
			{
				writer.WriteElementString(
					"Meta", true,
					"Name", player.DisplayNameOnlyDirectAndStripped,
					"Level", player.Statistics["Level"].BaseValue.ToString(),
					"Genotype", player.GetStringProperty("Genotype", ""),
					"Subtype", player.GetStringProperty("Subtype", ""),
					"Type", Grammar.MakeTitleCase(player.GetBlueprint()?.CachedDisplayNameStripped ?? ""),
					"Tile", player.pRender.Tile,
					"Foreground", player.pRender.GetMainColor().ToString(),
					"Detail", player.pRender.DetailColor,
					"Companions", String.Join(";", companions.Select(c => c.an(
						AsIfKnown: true, Single: true, NoConfusion: true,
						Stripped: true, /*TODO: WithoutEpithet: true, */BaseOnly: true
					)).ToArray()),
					"Time", The.Game.TimeTicks.ToString(),
					"Date", DateTime.Now.ToString("yyyyMMddHHmmss")
				);
			}
			catch (Exception e)
			{
				RLog("Error writing metadata", e);
			}
		}
	}
}
