using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using XRL;
using XRL.World;

using static Recur.Static;

namespace Recur.XML
{
	// What a bloody pain in the ass, half a mind to write a custom xml serializer to support arbitrary types.
	[XmlRoot("Part")]
	public class XMLCustomPart : XMLPart, IXmlSerializable
	{
		[XmlAttribute("Name")]
		public override string Name { get; set; }

		public IXmlSerializable Part { get; set; }

		public XMLCustomPart() { }
		public XMLCustomPart(IPart part) : base(part) {
			Part = part as IXmlSerializable;
		}

		public override void Apply(IPart part) {
			var obj = part.ParentObject;
			obj.RemovePart(part);
			Add(obj);
		}

		public override IPart Add(GameObject obj) {
			return obj.AddPart(Part as IPart);
		}

		public XmlSchema GetSchema() => null;

		public void WriteXml(XmlWriter writer) {
			writer.WriteAttributeString("Name", Name);
			Part.WriteXml(writer);
		}

		public void ReadXml(XmlReader reader) {
			reader.MoveToContent();
			Name = reader.GetAttribute("Name");
			try {
				var type = ModManager.ResolveType("XRL.World.Parts." + Name);
				Part = Activator.CreateInstance(type) as IXmlSerializable;
				Part.ReadXml(reader);
			} catch (Exception e) {
				RLog("Error reading custom part " + Name, e);
				reader.Skip();
			}
		}

	}
}
