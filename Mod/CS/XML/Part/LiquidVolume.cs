using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

using XRL.World;
using XRL.World.Parts;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Liquid")]
	public class XMLLiquidVolume : XMLPart
	{
		[XmlIgnore]
		public override string Name => "LiquidVolume";

		[XmlAttribute("Volume")]
		public int Volume { get; set; }

		[XmlElement("Component")]
		public List<XMLPair> Components = new List<XMLPair>();

		public XMLLiquidVolume() { }
		public XMLLiquidVolume(LiquidVolume liquid) : base(liquid) {
			Volume = liquid.Volume;
			liquid.ComponentLiquids.All(x => {
				Components.Add(new XMLPair(x.Key, x.Value.ToString()));
				return true;
			});
		}

		public override void Apply(IPart part) {
			var liquid = (LiquidVolume)part;
			liquid.Volume = Volume;
			liquid.ComponentLiquids.Clear();
			Components.All(x => {
				int value;
				if (Int32.TryParse(x.Value, out value))
					liquid.ComponentLiquids[x.Name] = value;
				//liquid.SetComponent(x.Name, value);
				return true;
			});
		}
	}
}
