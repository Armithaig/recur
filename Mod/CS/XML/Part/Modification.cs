using System;
using System.Xml;
using System.Xml.Serialization;

using XRL;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Capabilities;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Mod")]
	public class XMLModification : XMLPart
	{
		[XmlAttribute("Name")]
		public override string Name { get; set; }

		public XMLModification() { }
		public XMLModification(IModification mod) : base(mod) { }

		public override IPart Add(GameObject obj) {
			var type = ModManager.ResolveType("XRL.World.Parts." + Name);
			var mod = Activator.CreateInstance(type) as IModification;
			obj.ApplyModification(mod);
			return mod;
		}
	}
}
