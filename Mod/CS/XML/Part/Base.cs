using System;
using System.Xml.Serialization;

using XRL;
using XRL.World;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("BasePart")]
	public class XMLPart
	{
		// Recent serializer implementation change:
		// Now have to ignore this base field or its attributes will apply to all derived types, ignoring overrides.
		[XmlIgnore]
		public virtual string Name { get; set; }

		public XMLPart() { }
		public XMLPart(IPart part) {
			Name = part.Name;
		}

		public virtual void Apply(IPart part) { }
		public virtual IPart Add(GameObject obj) {
			var type = ModManager.ResolveType("XRL.World.Parts." + Name);
			var part = Activator.CreateInstance(type) as IPart;
			Apply(obj.AddPart(part));

			return part;
		}
	}
}
