using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

using XRL.World;
using XRL.World.Parts;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Description")]
	public class XMLDescription : XMLPart
	{
		[XmlIgnore]
		public override string Name => "Description";

		[XmlAttribute("Short")]
		public string Short { get; set; }

		[XmlAttribute("Mark")]
		[DefaultValue("")]
		public string Mark { get; set; } = string.Empty;

		public XMLDescription() { }
		public XMLDescription(Description desc) : base(desc) {
			Short = desc._Short;
			Mark = desc.Mark;
		}

		public override void Apply(IPart part) {
			var desc = (Description)part;
			desc._Short = Short;
			desc.Mark = Mark;
		}
	}
}
