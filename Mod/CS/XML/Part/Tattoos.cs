using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

using XRL.World;
using XRL.World.Parts;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Tattoos")]
	public class XMLTattoos : XMLPart
	{
		[XmlIgnore]
		public override string Name => "Tattoos";

		[XmlAttribute("Primary")]
		public string Primary { get; set; }

		[XmlAttribute("Secondary")]
		public string Secondary { get; set; }

		[XmlElement("Mark")]
		public List<XMLPair> Marks { get; set; } = new List<XMLPair>();

		public XMLTattoos() { }
		public XMLTattoos(Tattoos tattoos) : base(tattoos) {
			Primary = tattoos.ColorString;
			Secondary = tattoos.DetailColor;

			var body = tattoos.ParentObject.GetPart<Body>();

			foreach (var pair in tattoos.Descriptions) {
				var limb = body.GetPartByID(pair.Key, true);
				if (limb == null) continue;

				foreach (var desc in pair.Value) {
					Marks.Add(new XMLPair(limb.Description, desc));
				}
			}
		}

		public override void Apply(IPart part) {
			var tattoos = (Tattoos)part;
			tattoos.RemoveTattoos();
			tattoos.ColorString = Primary;
			tattoos.DetailColor = Secondary;

			var body = tattoos.ParentObject.GetPart<Body>();
			Marks.GroupBy(x => x.Name)
				.ToDictionary(k => k.Key, v => v.Select(pair => pair.Value))
				.All(pair => {
					var limb = body.GetPartByDescription(pair.Key);
					if (limb == null) return true;

					tattoos.Descriptions[limb.ID] = pair.Value.ToList();
					return true;
				});
		}
	}
}
