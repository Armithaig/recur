using System.Xml;
using System.Xml.Serialization;

using XRL.World;
using XRL.World.Parts;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Socket")]
	public class XMLEnergyCellSocket : XMLPart
	{
		[XmlIgnore]
		public override string Name => "EnergyCellSocket";

		[XmlElement("Cell")]
		public XMLGameObject Cell { get; set; }

		public XMLEnergyCellSocket() { }
		public XMLEnergyCellSocket(EnergyCellSocket socket) : base(socket) {
			if (socket.Cell != null)
				Cell = new XMLGameObject(socket.Cell);
		}

		public override void Apply(IPart part) {
			GameObject cell = null;
			var socket = (EnergyCellSocket)part;
			if (Cell != null) cell = Cell.Create();
			socket.Cell = cell;
		}
	}
}
