using System.Xml;
using System.Xml.Serialization;
using System.Linq;

using XRL.World;
using XRL.World.Parts;
using XRL.World.Tinkering;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Data")]
	public class XMLDataDisk : XMLPart
	{
		[XmlIgnore]
		public override string Name => "DataDisk";

		[XmlAttribute("Blueprint")]
		public string Blueprint { get; set; }

		public XMLDataDisk() { }
		public XMLDataDisk(DataDisk disk) : base(disk) {
			Blueprint = disk.Data.PartName;
		}

		public override void Apply(IPart part) {
			var recipe = TinkerData.TinkerRecipes.FirstOrDefault(x => x.PartName == Blueprint);
			if (recipe != null) {
				var disk = (DataDisk)part;
				disk.Data = recipe;
			}
		}
	}
}
