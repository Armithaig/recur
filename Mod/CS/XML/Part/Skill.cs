using System;
using System.Xml;
using System.Xml.Serialization;

using XRL;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Skill")]
	public class XMLSkill : XMLPart
	{
		[XmlAttribute("Name")]
		public override string Name { get; set; }

		public XMLSkill() { }
		public XMLSkill(BaseSkill skill) : base(skill) { }

		public override IPart Add(GameObject obj) {
			var type = ModManager.ResolveType("XRL.World.Parts.Skill." + Name);
			var skill = Activator.CreateInstance(type) as BaseSkill;
			obj.GetPart<Skills>().AddSkill(skill);
			return skill;
		}
	}
}
