using System;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

using XRL;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Mutation;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Mutation")]
	public class XMLMutation : XMLPart
	{
		[XmlAttribute("Name")]
		public override string Name { get; set; }

		[XmlAttribute("Constructor")]
		public string Constructor { get; set; }

		[XmlAttribute("Variant")]
		public string Variant { get; set; }

		[XmlAttribute("Level")]
		public int Level { get; set; }

		[XmlAttribute("RapidLevel")]
		[DefaultValue(0)]
		public int RapidLevel { get; set; }

		public XMLMutation() { }
		public XMLMutation(BaseMutation mutation) : base(mutation) {
			Level = mutation.BaseLevel;
			RapidLevel = mutation.GetRapidLevelAmount();
			Constructor = mutation.GetEntryByName()?.Constructor;
			Variant = mutation.Variant;
		}

		public override void Apply(IPart part) {
			var mutation = (BaseMutation)part;
			if (!string.IsNullOrEmpty(Variant)) SetVariant(mutation);
			SetLevel(mutation);
		}

		public override IPart Add(GameObject obj) {
			BaseMutation mutation;

			var type = ModManager.ResolveType("XRL.World.Parts.Mutation." + Name);
			if (string.IsNullOrEmpty(Constructor)) mutation = Activator.CreateInstance(type) as BaseMutation;
			else mutation = Activator.CreateInstance(type, Constructor.Split(new[] { ',' })) as BaseMutation;

			if (!string.IsNullOrEmpty(Variant)) SetVariant(mutation);

			// Add at base level and change it, adding level directly ain't applyin' right buffs.
			obj.GetPart<Mutations>().AddMutation(mutation, 1);
			SetLevel(mutation);
			return mutation;
		}

		private void SetLevel(BaseMutation mutation) {
			mutation.BaseLevel = Level;
			mutation.ChangeLevel(mutation.Level);

			if (RapidLevel > 0) mutation.RapidLevel(RapidLevel);
		}

		private void SetVariant(BaseMutation mutation) {
#if BUILD_2_0_204
			var variants = mutation.GetVariants();
			if (variants == null) return;

			var index = variants.IndexOf(Variant);
			if (index == -1) return;

			mutation.SetVariant(index);
#else
			mutation.SetVariant(Variant);
#endif
		}
	}
}
