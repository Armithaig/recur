using System.Xml;
using System.Xml.Serialization;
using ConsoleLib.Console;
using XRL.World;
using XRL.World.Parts;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Render")]
	public class XMLRender : XMLPart
	{
		[XmlIgnore]
		public override string Name => "Render";

		[XmlAttribute("DisplayName")]
		public string DisplayName { get; set; }

		[XmlAttribute("RenderString")]
		public string RenderString { get; set; }

		[XmlAttribute("ColorString")]
		public string ColorString { get; set; }

		[XmlAttribute("DetailColor")]
		public string DetailColor { get; set; }

		[XmlAttribute("TileColor")]
		public string TileColor { get; set; }

		[XmlAttribute("Tile")]
		public string Tile { get; set; }

		public XMLRender() { }
		public XMLRender(Render render) : base(render) {
			DisplayName = render.DisplayName;
			RenderString = render.RenderString;
			ColorString = render.ColorString;
			DetailColor = render.DetailColor;
			TileColor = render.TileColor;
			Tile = render.Tile;
		}

		public override void Apply(IPart part) {
			var render = (Render)part;
			render.DisplayName = DisplayName;
			render.RenderString = RenderString;
			render.ColorString = ColorString;
			render.DetailColor = DetailColor;
			render.TileColor = TileColor;
			render.Tile = Tile;
		}

		public char? GetForeground()
		{
			return ColorUtility.FindLastForeground(TileColor)
			       ?? ColorUtility.FindLastForeground(ColorString);
		}
	}
}
