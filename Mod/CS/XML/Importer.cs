using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using XRL.World.Parts;
using static Recur.Static;

namespace Recur.XML
{
	public static class Importer
	{
		public static bool Import(Aspect aspect)
		{
			if (!aspect.File.Exists) return false;
			if (aspect.Player != null) return true;

			var srl = XmlSerializer.FromTypes(new[] { typeof(XMLGameObject), typeof(XMLReputation), typeof(XMLTinker), typeof(XMLJournal) });
			var reader = XmlReader.Create(
				aspect.File.FullName,
				new XmlReaderSettings
				{
					CloseInput = true,
					CheckCharacters = false,
					ConformanceLevel = ConformanceLevel.Fragment
				});

			try
			{
				while (reader.Read())
				{
					if (reader.LocalName.ToLower() == "gameobject")
					{
						var obj = (XMLGameObject) srl[0].Deserialize(reader);
						if (obj.Character == "Player")
						{
							aspect.Player = obj;
							aspect.Render = obj.Parts.FirstOrDefault(x => x is XMLRender) as XMLRender;
						}
						else
						{
							aspect.Companions.Add(obj);
						}
					}

					if (reader.LocalName.ToLower() == "reputation")
					{
						aspect.Reputation = (XMLReputation) srl[1].Deserialize(reader);
					}

					if (reader.LocalName.ToLower() == "tinker")
					{
						aspect.Tinker = (XMLTinker) srl[2].Deserialize(reader);
					}

					if (reader.LocalName.ToLower() == "journal")
					{
						aspect.Journal = (XMLJournal) srl[3].Deserialize(reader);
					}
				}

				return true;
			}
			catch (Exception e)
			{
				RLog("Error deserialising data of file " + aspect.File.Name, e);
			}
			finally
			{
				reader.Close();
			}

			return false;
		}

		public static List<Aspect> ReadMetaData()
		{
			var result = new List<Aspect>();
			var dir = CharDir;
			dir.Refresh();
			if (!dir.Exists) return result;

			var settings = new XmlReaderSettings
			{
				ConformanceLevel = ConformanceLevel.Fragment,
				CloseInput = true
			};

			foreach (var file in dir.GetFiles("*.xml", SearchOption.AllDirectories))
			{
				var meta = new Dictionary<string, string> { { "Name", file.Name } };
				var reader = XmlReader.Create(file.OpenRead(), settings);
				try
				{
					if (reader.Read() && reader.LocalName.ToLower() == "meta")
					{
						while (reader.MoveToNextAttribute())
							meta[reader.LocalName] = reader.Value;
					}

					result.Add(new Aspect(file, meta));
				}
				catch (Exception e)
				{
					RLog("Error reading metadata of file " + file.Name, e);
				}
				finally
				{
					reader.Close();
				}
			}

			return result;
		}
	}
}
