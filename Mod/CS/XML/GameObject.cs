using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Mutation;
using XRL.World.Parts.Skill;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("GameObject")]
	public class XMLGameObject
	{
		[XmlAttribute("Blueprint")]
		public string Blueprint { get; set; }

		[XmlAttribute("Character")]
		public string Character { get; set; }

		[XmlAttribute("Source")]
		public string Source { get; set; }

		// Legacy, apply only
		[XmlAttribute("Gender")]
		public string GenderName { get; set; }

		// Legacy, apply only
		[XmlAttribute("Equipped")]
		public string Equipped { get; set; }

		// Legacy, apply only
		[XmlAttribute("Implanted")]
		public string Implanted { get; set; }

		[XmlAttribute("Feeling")]
		[DefaultValue(0)]
		public int Feeling { get; set; }

		[XmlAttribute("Number")]
		[DefaultValue(1)]
		public int Number { get; set; } = 1;

		[XmlElement(typeof(XMLGender)),
		XmlElement(typeof(XMLPronounSet))]
		public List<XMLPronominal> Pronominals { get; set; } = new List<XMLPronominal>();

		[XmlElement(typeof(XMLRender)),
		XmlElement(typeof(XMLDescription)),
		XmlElement(typeof(XMLMutation)),
		XmlElement(typeof(XMLSkill)),
		XmlElement(typeof(XMLLiquidVolume)),
		XmlElement(typeof(XMLEnergyCellSocket)),
		XmlElement(typeof(XMLModification)),
		XmlElement(typeof(XMLDataDisk)),
		XmlElement(typeof(XMLTattoos)),
		XmlElement("Part", typeof(XMLCustomPart))]
		public List<XMLPart> Parts { get; set; } = new List<XMLPart>();

		[XmlElement("Statistic")]
		public List<XMLStatistic> Statistics { get; set; } = new List<XMLStatistic>();

		[XmlElement("Property")]
		public List<XMLPair> Properties { get; set; } = new List<XMLPair>();

		[XmlElement("IntProperty")]
		public List<XMLPair> IntProperties { get; set; } = new List<XMLPair>();

		public XMLBody Body { get; set; }

		[XmlElement("Item")]
		public List<XMLGameObject> Items { get; set; } = new List<XMLGameObject>();

		public XMLGameObject() { }

		/// <summary>
		/// Create XML serialisable game object.
		/// </summary>
		/// <param name="unique">Whether this game object differs significantly from its blueprint.</param>
		public XMLGameObject(GameObject obj, bool unique = false) {
			Blueprint = obj.Blueprint;
			Number = obj.Count;

			if (obj?.pBrain?.PartyLeader != null)
				Feeling = obj.pBrain.GetPersonalFeeling(obj.pBrain.PartyLeader) ?? 0;

			if (unique) {
				Character = obj.IsPlayer() ? "Player" : "Companion";
				if (obj.HasEffect("Beguiled")) Source = "Beguile";
				else if (obj.HasEffect("Proselytized")) Source = "Proselytize";

				try {
					var gender = obj.GetGender();
					if (gender != null) Pronominals.Add(new XMLGender(gender));

					var set = obj.GetPronounSet();
					if (set != null) Pronominals.Add(new XMLPronounSet(set));
				} catch (Exception e) {
					RLog("Error writing pronominals", e);
				}

				Parts.Add(new XMLRender(obj.pRender));
				Parts.Add(new XMLDescription(obj.GetPart<Description>()));

				var tattoos = obj.GetPart<Tattoos>();
				if (tattoos != null) Parts.Add(new XMLTattoos(tattoos));

				var body = obj.GetPart<Body>();
				if (body != null) Body = new XMLBody(body);

				obj.GetPartsDescendedFrom<BaseMutation>()
				   .OrderBy(x => x.Name)
				   .All(x => {
					   try { Parts.Add(new XMLMutation(x)); } catch (Exception e) {
						   RLog("Error writing mutation " + x.Name, e);
					   }
					   return true;
				   });

				obj.GetPartsDescendedFrom<BaseSkill>()
				   .OrderBy(x => x.Name)
				   .All(x => {
					   try { Parts.Add(new XMLSkill(x)); } catch (Exception e) {
						   RLog("Error writing skill " + x.Name, e);
					   }
					   return true;
				   });

				var diffs = GetBaseValueDiff(obj);
				var points = new[] { "SP", "MP", "AP" };
				var blacklist = new[] { "Energy" };
				obj.Statistics.Values.All(x => {
					try {
						if (blacklist.Contains(x.Name))
							return true;

						var value = x.BaseValue - diffs[x.Name];
						var stat = new XMLStatistic(x.Name, value.ToString());
						if (points.Contains(x.Name))
							stat.Penalty = x.Penalty.ToString();

						Statistics.Add(stat);
					} catch (Exception e) {
						RLog("Error writing stat " + x.Name, e);
					}
					return true;
				});

				GetProperties(obj, "MutationLevel", "BodyForm", "Genotype", "Species",
					"Subtype", "ProperNoun", "TurnsAsPlayerMinion", "Renamed");
			}

			obj.GetPart<Inventory>()?.ForeachObject(x => {
				// Skip serialising natural equipment.
				if (x.GetIntProperty("Natural") > 0) return;

				try { Items.Add(new XMLGameObject(x)); } catch (Exception e) {
					RLog("Error writing inventory item " + x.Blueprint, e);
				}
			});

			var liquid = obj.GetPart<LiquidVolume>();
			if (liquid != null) Parts.Add(new XMLLiquidVolume(liquid));

			var socket = obj.GetPart<EnergyCellSocket>();
			if (socket != null) Parts.Add(new XMLEnergyCellSocket(socket));

			var disk = obj.GetPart<DataDisk>();
			if (disk != null) Parts.Add(new XMLDataDisk(disk));

			obj.GetPartsDescendedFrom<IModification>()
			   .OrderBy(x => x.Name)
			   .All(x => {
				   try { Parts.Add(new XMLModification(x)); } catch (Exception e) {
					   RLog("Error writing modification " + x.Name, e);
				   }
				   return true;
			   });

			obj.PartsList.Where(x => typeof(IXmlSerializable).IsAssignableFrom(x.GetType()))
				.OrderBy(x => x.Name)
				.All(x => {
					try { Parts.Add(new XMLCustomPart(x)); } catch (Exception e) {
						RLog("Error writing custom part " + x.Name, e);
					}
					return true;
				});
		}

		public void Apply(GameObject obj) {
			var body = obj.GetPart<Body>();

			if (!String.IsNullOrEmpty(GenderName) && Gender.Exists(GenderName))
				obj.SetGender(GenderName);

			Pronominals.All(x => {
				try { x.Apply(obj); } catch (Exception e) {
					RLog("Error applying pronominal " + x.Name, e);
				}
				return true;
			});

			try { if (body != null) Body?.Apply(body); } catch (Exception e) {
				RLog("Error applying body", e);
			}

			// Apply/override these stats last since they're influenced by priors e.g. con/hp, int/sp
			var last = new[] { "XP", "SP", "Hitpoints" };
			Statistics.OrderBy(x => last.Contains(x.Name)).All(x => {
				try {
					var stat = obj.Statistics[x.Name];
					stat.BaseValue = Int32.Parse(x.Value);
					if (!String.IsNullOrEmpty(x.Penalty))
						stat.Penalty = Int32.Parse(x.Penalty);
				} catch (Exception e) {
					RLog("Error applying stat " + x.Name, e);
				}
				return true;
			});

			Parts.All(x => {
				try {
					var part = obj.GetPart(x.Name);
					if (part != null) x.Apply(part);
					else x.Add(obj);
				} catch (Exception e) {
					RLog("Error applying part " + x.Name, e);
				}
				return true;
			});

			Properties.All(x => {
				if (x.Name != null)
					obj.Property[x.Name] = x.Value;
				return true;
			});

			IntProperties.All(x => {
				int num = 0;
				if (x.Name != null && Int32.TryParse(x.Value, out num))
					obj.IntProperty[x.Name] = num;
				return true;
			});

			if (body != null && Body != null) {
				try { Body.Find(); } catch (Exception e) {
					RLog("Error finding body parts", e);
				}

				try { Body.Dismember(body); } catch (Exception e) {
					RLog("Error dismembering body parts", e);
				}
			}
		}

		// Have to separate this and generate on/after world gen/cell entrance or stats'll be inaccurate.
		public void ApplyItems(GameObject obj) {
			ClearItems(obj);
			Body?.Equip();

			if (!obj.HasPart("Inventory")) return;

			var body = obj.GetPart<Body>();
			Items.All(x => {
				try {
					var item = x.Create();

					if (x.Items.Count > 0) x.ApplyItems(item);
					// Legacy equipment load
					if (body != null) {
						if (x.Implanted != null) {
							var part = body.GetPartByDescription(x.Implanted);
							if (part != null) {
								part.Implant(item);
								return true;
							}
						} else if (x.Equipped != null) {
							var part = body.GetPartByDescription(x.Equipped);
							if (part != null) {
								eCommandEquipObject.SetParameter("Object", item);
								eCommandEquipObject.SetParameter("BodyPart", part);
								if (obj.FireEvent(eCommandEquipObject))
									return true;
							}
						}
					}

					if (x.Number > 1) {
						var stack = item.GetPart<Stacker>();
						if (stack != null) stack.StackCount = x.Number;
					}

					eCommandTakeObject.SetParameter("Object", item);
					obj.FireEvent(eCommandTakeObject);
				} catch (Exception e) {
					RLog("Error creating item " + x.Blueprint, e);
				}
				return true;
			});
		}

		public GameObject Create() {
			var factory = GameObjectFactory.Factory;
			var blueprint = Blueprint;

			if (Character != null && !factory.Blueprints.ContainsKey(blueprint)) {
				blueprint = "Creature";
				RLog("Blueprint " + Blueprint + " not found, defaulting to Creature.");
			}
#if BUILD_2_0_204
			return factory.CreateObject(blueprint, -9999, beforeObjectCreated: Apply);
#else
			return factory.CreateObject(blueprint, -9999, BeforeObjectCreated: Apply);
#endif
		}

		public string GetProperty(string Key, string Default = null)
		{
			foreach (var property in Properties)
			{
				if (property.Name == Key) return property.Value;
			}

			return Default;
		}

		public string GetDisplayName()
		{
			foreach (var part in Parts)
			{
				if (part is XMLRender render)
				{
					return render.DisplayName;
				}
			}

			return null;
		}

		private void GetProperties(GameObject obj, params string[] names) {
			string str; int num;
			foreach (var name in names) {
				if (obj.Property.TryGetValue(name, out str))
					Properties.Add(new XMLPair(name, str));
				else if (obj.IntProperty.TryGetValue(name, out num))
					IntProperties.Add(new XMLPair(name, num.ToString()));
			}
		}
	}
}
