using System.Xml;
using System.Xml.Serialization;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Pair")]
	public class XMLPair
	{
		[XmlAttribute("Name")]
		public string Name { get; set; }

		[XmlAttribute("Value")]
		public string Value { get; set; }

		public XMLPair() { }
		public XMLPair(string name) {
			Name = name;
		}
		public XMLPair(string name, string value) : this(name) {
			Value = value;
		}
	}

	[XmlType("Statistic")]
	public class XMLStatistic : XMLPair
	{
		[XmlAttribute("Penalty")]
		public string Penalty { get; set; }

		public XMLStatistic() { }
		public XMLStatistic(string name, string value) : base(name, value) { }
		public XMLStatistic(string name, string value, string penalty) : this(name, value) {
			Penalty = penalty;
		}
	}
}
