
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using XRL;
using XRL.Core;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Reputation")]
	public class XMLReputation
	{
		[XmlElement("Faction")]
		public List<XMLPair> Factions { get; set; } = new List<XMLPair>();

		public XMLReputation() { }
		public XMLReputation(bool auto) {
			var rep = The.Game.PlayerReputation;
			var values = rep.GetType().GetField("ReputationValues", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(rep);
			StringifyObjectionary(values)
				.Where(x => !x.Key.Contains("SultanCult"))
				.OrderBy(x => x.Key)
				.All(x => {
					Factions.Add(new XMLPair(x.Key, x.Value));
					return true;
				});
		}

		public void Apply() {
			var rep = The.Game.PlayerReputation;
			foreach (var pair in Factions) {
				try {
					int opinion;
					if (Int32.TryParse(pair.Value, out opinion))
						rep.set(pair.Name, opinion);
				} catch (Exception e) {
					RLog("Error loading faction " + pair.Name, e);
				}
			}
		}
	}
}
