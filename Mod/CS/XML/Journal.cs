using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Qud.API;
using XRL;
using XRL.World.Parts;
using static Recur.Static;
using static Qud.API.JournalAccomplishment;

namespace Recur.XML
{
	[XmlType("Journal")]
	public class XMLJournal
	{
		[XmlAttribute("Time")] public long Time { get; set; }

		[XmlArrayItem("Entry")]
		[XmlArray("Accomplishments")]
		public List<XMLJournalEntry> Accomplishments { get; set; } = new List<XMLJournalEntry>();

		[XmlArrayItem("Entry")]
		[XmlArray("General")]
		public List<XMLJournalEntry> General { get; set; } = new List<XMLJournalEntry>();

		public XMLJournal()
		{
		}

		public XMLJournal(bool auto) : this(JournalAPI.Accomplishments, JournalAPI.GeneralNotes, The.Game.TimeTicks)
		{
		}

		public XMLJournal(List<JournalAccomplishment> accomplishments, List<JournalGeneralNote> general, long time)
		{
			Time = time;

			Accomplishments.AddRange(accomplishments.GroupBy(x => new { x.time, x.text })
				.Select(x => new XMLJournalEntry(x.First()))
				.OrderBy(x => x.Time)
			);

			// Add journal entry hinting of how ya might've ended up back in some village (in another dimension?).
			var papaya = PersistentPapaya.GetAccomplishment();
			if (papaya != null)
				Accomplishments.Add(new XMLJournalEntry(papaya));

			General.AddRange(general.OrderBy(x => x.time)
				.Select(x => new XMLJournalEntry(x))
			);
		}

		public void Apply()
		{
			The.Game.TimeTicks += Time;

			Accomplishments.OrderBy(x => x.Time)
				.All(x =>
				{
					var mrlCat = MuralCategory.Generic;
					try
					{
						mrlCat = (MuralCategory) Enum.Parse(mrlCat.GetType(), x.MuralCategory, true);
					}
					catch (Exception e)
					{
						RLog("Could not parse mural category " + x.MuralCategory, e);
					}

					var mrlWgt = MuralWeight.Medium;
					try
					{
						mrlWgt = (MuralWeight) Enum.Parse(mrlWgt.GetType(), x.MuralWeight, true);
					}
					catch (Exception e)
					{
						RLog("Could not parse mural weight " + x.MuralWeight, e);
					}

					JournalAPI.AddAccomplishment(text: x.Text, muralText: x.MuralText, category: x.Category, muralCategory: mrlCat, muralWeight: mrlWgt, time: x.Time);
					return true;
				});

			General.OrderBy(x => x.Time)
				.All(x =>
				{
					JournalAPI.AddGeneralNote(text: x.Text, time: x.Time);
					return true;
				});
		}
	}

	[XmlType("Entry")]
	public class XMLJournalEntry
	{
		[XmlAttribute("Text")] public string Text { get; set; }

		[XmlAttribute("Category")]
		[DefaultValue("general")]
		public string Category { get; set; } = "general";

		[XmlAttribute("MuralText")] public string MuralText { get; set; }

		[XmlAttribute("MuralCategory")]
		[DefaultValue("Generic")]
		public string MuralCategory { get; set; } = "Generic";

		[XmlAttribute("MuralWeight")]
		[DefaultValue("Medium")]
		public string MuralWeight { get; set; } = "Medium";

		[XmlAttribute("Time")]
		[DefaultValue(-1L)]
		public long Time { get; set; } = -1L;


		public XMLJournalEntry()
		{
		}

		public XMLJournalEntry(JournalAccomplishment entry)
		{
			Text = entry.text;
			Category = entry.category;
			MuralText = entry.muralText;
			MuralCategory = entry.muralCategory.ToString();
			MuralWeight = entry.muralWeight.ToString();
			Time = entry.time;
		}

		public XMLJournalEntry(JournalGeneralNote entry)
		{
			Text = entry.text;
			Time = entry.time;
		}
	}
}
