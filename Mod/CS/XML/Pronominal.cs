using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using XRL.World;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("Pronominal")]
	public class XMLPronominal
	{
		[XmlAttribute("Name")]
		public string Name;

		[XmlElement("Entry")]
		public List<XMLPair> Entries = new List<XMLPair>();

		[XmlElement("Option")]
		public List<XMLPair> Options = new List<XMLPair>();

		public XMLPronominal() { }
		public XMLPronominal(IPronounProvider provider) {
			Name = provider.Name;
			if (!provider.Generated) return;

			provider.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).All(x => {
				if (x.IsDefined(typeof(NonSerializedAttribute), true)) return true;

				var value = x.GetValue(provider);
				var name = x.Name.TrimStart('_');
				if (value == null || name == "Name") return true;

				if (x.FieldType == typeof(string))
					Entries.Add(new XMLPair(name, (string)value));
				else if (x.FieldType == typeof(bool))
					Options.Add(new XMLPair(name, value.ToString()));

				return true;
			});
		}

		public void Apply(IPronounProvider provider) {
			if (provider == null) return;
			var type = provider.GetType();

			Entries.Concat(Options)
				.All(x => {
					var field = type.GetField("_" + x.Name) ?? type.GetField(x.Name);
					if (field == null) return true;

					bool option;
					if (field.FieldType == typeof(string))
						field.SetValue(provider, x.Value);
					else if (field.FieldType == typeof(bool) && bool.TryParse(x.Value, out option))
						field.SetValue(provider, option);

					return true;
				});
		}
		public virtual void Apply(GameObject obj) {
			Apply(obj.GetPronounProvider());
		}
	}

	[XmlType("Gender")]
	public class XMLGender : XMLPronominal
	{
		public XMLGender() { }
		public XMLGender(Gender gender) : base(gender) { }

		public override void Apply(GameObject obj) {
			if (Gender.Exists(Name))
				obj.SetGender(Name);
			else if (Entries.Count > 0) {
				var gender = new Gender(Name);
				Apply(gender);
				obj.SetGender(gender.Register());
			}
		}
	}

	[XmlType("PronounSet")]
	public class XMLPronounSet : XMLPronominal
	{
		public XMLPronounSet() { }
		public XMLPronounSet(PronounSet set) : base(set) { }

		public override void Apply(GameObject obj) {
			if (PronounSet.Exists(Name))
				obj.SetPronounSet(Name);
			else if (Entries.Count > 0) {
				var set = new PronounSet();
				Apply(set);
				obj.SetPronounSet(set.Register());
			}
		}
	}
}
