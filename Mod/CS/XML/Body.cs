using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

using XRL.UI;
using XRL.World;
using XRL.World.Anatomy;
using XRL.World.Parts;
using CapLat = XRL.World.Capabilities.Laterality;

using static Recur.Static;

namespace Recur.XML
{
	[XmlType("BodyPart")]
	public class XMLBodyPart
	{
		[XmlIgnore]
		public BodyPart Part { get; set; }

		[XmlIgnore]
		public int Position { get; set; }

		[XmlAttribute("Type")]
		[DefaultValue("Body")]
		public string Type { get; set; } = "Body";

		[XmlAttribute("Laterality")]
		public string Laterality { get; set; }

		[XmlAttribute("Category")]
		public string Category { get; set; }

		[XmlAttribute("Mobility")]
		[DefaultValue(int.MinValue)]
		public int Mobility { get; set; } = int.MinValue;

		[XmlAttribute("Support")]
		public string Support { get; set; }

		[XmlAttribute("Depend")]
		public string Depend { get; set; }

		[XmlAttribute("Origin")]
		[DefaultValue("Native")]
		public string Origin { get; set; } = "Native";

		[XmlAttribute("Primary")]
		[DefaultValue("No")]
		public string Primary { get; set; } = "No";

		[XmlAttribute("Dismembered")]
		[DefaultValue("No")]
		public string Dismembered { get; set; } = "No";

		[XmlElement("Item")]
		public XMLGameObject Item { get; set; }

		[XmlElement("Implant")]
		public XMLGameObject Implant { get; set; }

		[XmlElement("Behavior")]
		public XMLGameObject Behavior { get; set; }

		[XmlElement("Part")]
		public List<XMLBodyPart> Children { get; set; } = new List<XMLBodyPart>();

		public XMLBodyPart() { }
		public XMLBodyPart(Body body, BodyPart part, bool dismembered = false) {
			Type = part.VariantType;
			Support = part.SupportsDependent;
			Depend = part.DependsOn;
			Position = part.Position;
			Laterality = CapLat.LateralityAdjective(part.Laterality, true);
			Dismembered = dismembered ? "Yes" : "No";

			if (!part.Native) {
				if (part.Manager != null) Origin = "Managed";
				else Origin = "Foreign";
			}

			var variant = part.VariantTypeModel();
			if (part.Mobility != (variant.Mobility ?? 0)) Mobility = part.Mobility;
			if (part.Category != (variant.Category ?? 1)) Category = BodyPartCategory.GetName(part.Category);

			if (!dismembered) {
#if BUILD_2_0_203
				Primary = part.PreferedPrimary ? "Yes" : "No";
#else
				Primary = part.PreferredPrimary ? "Yes" : "No";
#endif
				if (part.Equipped != null) Item = new XMLGameObject(part.Equipped);
				if (part.Cybernetics != null) Implant = new XMLGameObject(part.Cybernetics);
				if (part.DefaultBehavior != null && part.DefaultBehavior.Blueprint != variant.DefaultBehavior)
					Behavior = new XMLGameObject(part.DefaultBehavior);
			}

			part.Parts?.ForEach(x => Children.Add(new XMLBodyPart(body, x)));
			body.DismemberedParts?.ForEach(x => {
				if (!part.idMatch(x.ParentID)) return;

				Children.Add(new XMLBodyPart(body, x.Part, true));
			});

			Children.Sort((a, b) => a.Position.CompareTo(b.Position));
		}

		/// <summary>
		/// Create and add this body part and any children.
		/// </summary>
		public virtual void Apply(Body body) {
			var code = string.IsNullOrEmpty(Laterality) ? CapLat.NONE : CapLat.GetCode(Laterality);
			Part = new BodyPart(
				Base: Type,
				Laterality: code,
				ParentBody: body,
				SupportsDependent: Support,
				DependsOn: Depend
			);

			Part.Native = Origin.EqualsNoCase("Native");

			if (Mobility != int.MinValue) Part.Mobility = Mobility;
			if (Category != null) Part.Category = BodyPartCategory.GetCode(Category);
			if (Part.Parts == null) Part.Parts = new List<BodyPart>(Children.Count);
			if (body._Body == null) body._Body = Part;

			Children.ForEach(x => {
				if (x.Origin.EqualsNoCase("Managed")) return;

				x.Apply(body);
				Part.AddPart(NewPart: x.Part);
			});
		}

		/// <summary>
		/// Find matching body part if not previously created so we can equip to it.
		/// </summary>
		public virtual void Find(BodyPart parent = null) {
			if (Part == null) {
				if (parent?.Parts == null) return;

				var variant = Anatomies.GetBodyPartTypeOrFail(Type);
				var code = string.IsNullOrEmpty(Laterality) ? CapLat.NONE : CapLat.GetCode(Laterality);
				var managed = Origin.EqualsNoCase("Managed");

				foreach (var part in parent.Parts) {
					if (part.Type != variant.FinalType) continue;
					if (managed && (part.Manager == null || part.Native)) continue;
					if (code != part.Laterality) continue; // TODO: Slight fuzz around laterality
					Part = part;
					break;
				}

				if (Part == null) return;
			}

			Children.ForEach(x => x.Find(Part));
		}

		/// <summary>
		/// Remove stale part references from previous build attempt.
		/// </summary>
		public void Reset() {
			Part = null;
			Children.ForEach(x => x.Reset());
		}

		/// <summary>
		/// Dismember body parts, run after mutations added.
		/// </summary>
		public virtual void Dismember(Body body) {
			if (Part == null) return;

			if (Dismembered.EqualsNoCase("Yes"))
				body.CutAndQueueForRegeneration(Part);

			Children.ForEach(x => x.Dismember(body));
		}

		/// <summary>
		/// Equip body part with defined item, cybernetic or default behavior.
		/// </summary>
		public virtual void Equip() {
			EquipItem();
			EquipBehavior();
		}

		public IEnumerable<XMLBodyPart> YieldParts()
		{
			yield return this;
			foreach (var child in Children)
			foreach (var descendant in child.YieldParts())
			{
				yield return descendant;
			}
		}

		protected virtual void EquipItem() {
			if (Part == null || Dismembered.EqualsNoCase("Yes")) return;

			if (Assignable(Item, Part.Equipped)) {
				try {
					eCommandEquipObject.SetParameter("Object", Item.Create());
					eCommandEquipObject.SetParameter("BodyPart", Part);
					Part.ParentBody.ParentObject.FireEvent(eCommandEquipObject);
				} catch (Exception e) {
					RLog("Error equipping item " + Item.Blueprint, e);
				}
			} /*else if (Item != null) {
				RLog($"Skipping {Part.Description}: {Part.Equipped?.Blueprint} == {Item.Blueprint}");
			}*/

			if (Assignable(Implant, Part.Cybernetics)) {
				try {
					Part.Implant(Implant.Create());
				} catch (Exception e) {
					RLog("Error implanting cybernetic " + Implant.Blueprint, e);
				}
			} /*else if (Implant != null) {
				RLog($"Skipping {Part.Description}: {Part.Cybernetics?.Blueprint} == {Implant.Blueprint}");
			}*/

			if (Primary.EqualsNoCase("Yes"))
				Part.SetAsPreferredDefault(true);

			Children.ForEach(x => x.EquipItem());
		}

		// Mostly set from cybernetics & mutations, which we don't want to interfere with.
		// Assign after those, prefer existing object of same blueprint.
		protected virtual void EquipBehavior() {
			if (Part == null) return;

			if (Assignable(Behavior, Part.DefaultBehavior)) {
				try {
					Part.DefaultBehavior = Behavior.Create();
				} catch (Exception e) {
					RLog("Error assigning default behavior " + Behavior.Blueprint, e);
				}
			} /*else if (Behavior != null) {
				RLog($"Skipping {Part.Description}: {Part.DefaultBehavior?.Blueprint} == {Behavior.Blueprint}");
			}*/

			Children.ForEach(x => x.EquipBehavior());
		}

		protected bool Assignable(XMLGameObject xml, GameObject obj) {
			if (xml == null) return false;
			if (obj == null) return true;
			return xml.Blueprint != obj.Blueprint;
		}
	}

	[XmlType("Body")]
	public class XMLBody : XMLBodyPart
	{
		[XmlAttribute("Anatomy")]
		public string Anatomy { get; set; }

		public XMLBody() { }
		public XMLBody(Body body) : base(body, body._Body) {
			var original = body.ParentObject.GetBlueprint().GetPartParameter<string>("Body", "Anatomy");
			if (body.Anatomy != original) Anatomy = body.Anatomy;
		}

		/// <summary>
		/// Create and add this body to creature.
		/// </summary>
		public override void Apply(Body body) {
			try {
				if (ImportBody && Children.Count > 0) {
					body.built = false;
					body.DismemberedParts = null;
					body._Body = null;
					base.Apply(body);
					CorrectLaterality(body._Body);
					body.built = true;
					body.UpdateBodyParts();
					return;
				}
			} catch (Exception e) {
				RLog("Error recreating detailed body structure, defaulting to anatomy", e);
				Reset();
			}

			body.Anatomy = Anatomy ?? body.Anatomy;
			Part = body._Body;
		}
	}
}

// backwards compat
namespace XRL.World.Anatomy
{
	
}
