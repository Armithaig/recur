﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Qud.API;
using XRL.UI;
using XRL.Core;
using XRL.Language;

using Recur.XML;
using static Recur.Static;
using static Qud.API.JournalAccomplishment;

namespace XRL.World.Parts
{
	[Serializable]
	public class PersistentPapaya : IPart
	{
		public Dictionary<string, object> Fields = new Dictionary<string, object>();

		static readonly string[] messages = new[] {
			"&YAs you munch on the luscious insides of the colorful ovaloid you are {0} by {1}&Y {2}, filling you with persistence.",
			"&YCracking the brine-hardened shell of the fruit, you bite into its supple flesh {0} by {1}&Y {2}.\n\nIt fills you with persistence.",
			"&YWhile consuming the succulent treat your mind's eye is overcome with visions of {1}&Y {2}.\n\nYou feel {0} to remain persistent.",
			"&YThe peculiar ovaliform splits under your {0} efforts, a cacophanous noise of {1}&Y {2} suddenly rings in your head.\n\nUnperturbed, you eat and persist.",
			"&YWhilst reaping its vibrant guts for your own subsistence, a wordless voice speaks to you of the many {1}&Y that must be released from their mortal coils; to no longer pursue {2}.\n\n"
				+ "Ignoring the {0} but fruity voice fills you with persistence."
		};

		static readonly string[] accomplishments = new[] {
			"You spent some time {1} with {0}&y.",
			"You carefully studied {0}&y as they were {1}.",
			"You joined a group of {0}&y in {1}.",
			"Kidnapped by {0}&y, you learn much of {1}.",
			"Some {0}&y took you in as their own, teaching you of {1}.",
			"A band of {0}&y silently observed you regurgitating vibrant hues, postponing {1}."
		};

		static readonly string[] inspired = new[] {
			"inspired",
			"encouraged",
			"roused",
			"energized",
			"inspirited"
		};

		public override bool AllowStaticRegistration() {
			return true;
		}

		public override void Register(GameObject obj) {
			obj.RegisterPartEvent(this, "GetInventoryActions");
			obj.RegisterPartEvent(this, "InvCommandEatObject");
			PapayaColors(obj);
			base.Register(obj);
		}

		public override bool FireEvent(Event E) {
			if (E.ID == "GetInventoryActions") {
				var actions = E.GetParameter("Actions") as EventParameterGetInventoryActions;
				actions.AddAction("Eat", 'a', false, "e&Wa&yt", "InvCommandEatObject");
			} else if (E.ID == "InvCommandEatObject") {
				var owner = E.GetGameObjectParameter("Owner");
				var inventory = owner.GetPart<Inventory>();
				var obj = ParentObject.RemoveOne();
				try {
					inventory?.RemoveObject(obj);
					if (Exporter.Export()) {
						ShowMessage(owner);
						obj.Destroy();
						return true;
					}
				} catch (Exception e) {
					inventory?.AddObject(obj, Silent: true);
					RLog("Error while exporting", e);
				}
				Popup.Show("You bring your very existence to bear on the detestably steadfast oblong; it rejects you, everything you stand for, and your entire bedeviled lineage.");
			}
			return base.FireEvent(E);
		}

		public static GameObject GetRandomCreature() {
			return GameObjectFactory.Factory.BlueprintList.GetRandomElement(x => {
				if (x.HasTag("ExcludeFromDynamicEncounters")) return false;
				if (x.HasTag("BaseObject")) return false;
				if (x.HasPart("GivesRep")) return false;
				if (x.GetxTag("Grammar", "Proper") != null) return false;
				if (x.GetxTag("TextFragments", "Activity") == null) return false;
				return true;
			}, RANDOM)?.createSample();
		}

		public static void ShowMessage(GameObject obj) {
			if (obj == null || !obj.IsPlayer()) return;

			// Rolled from appropriate dynamic table for region at first but..
			// Way funnier when's completely random.
			var creature = GetRandomCreature();
			if (creature == null) return;

			var activity = creature.GetxTag_CommaDelimited("TextFragments", "Activity", "staring at the Beetle Moon");
			Popup.Show(String.Format(messages.GetRandomElement(RANDOM), inspired.GetRandomElement(RANDOM), Grammar.Pluralize(creature.DisplayName), activity));
			creature.Destroy();
		}

		public static JournalAccomplishment GetAccomplishment() {
			var creature = GetRandomCreature();
			if (creature == null) return null;

			var activity = creature.GetxTag_CommaDelimited("TextFragments", "Activity", "staring at the Beetle Moon");
			var result = new JournalAccomplishment {
				text = String.Format(accomplishments.GetRandomElement(RANDOM), Grammar.Pluralize(creature.DisplayName), activity),
				category = "general",
				muralCategory = MuralCategory.WeirdThingHappens,
				time = The.Game.TimeTicks
			};
			creature.Destroy();
			return result;
		}

		public static void PapayaColors(GameObject obj) {
			var sb = new StringBuilder(obj.GetBlueprint().GetPartParameter<string>("Render", "DisplayName"));
			sb.Insert(0, "&Opersistent ");
			sb.Append("&y");
			new[] { "&G", "&W", "&o" }.All(x => {
				int i = 0;
				do i = RANDOM.Next(2, sb.Length - 3);
				while (sb[i - 1] == '&');
				sb.Insert(i, x);
				return true;
			});
			obj.DisplayName = sb.ToString();
		}
	}
}
