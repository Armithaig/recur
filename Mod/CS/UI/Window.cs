﻿using System.Collections.Generic;
using System.Linq;
using Recur.XML;
using UnityEngine;
using XRL.CharacterBuilds;
using XRL.CharacterBuilds.Qud;
using XRL.CharacterBuilds.UI;
using XRL.UI;
using XRL.UI.Framework;

namespace Recur.UI
{
    [UIView("Recur:Import", NavCategory = "Menu", UICanvas = "Recur/Import", UICanvasHost = 1)]
    public class Window : EmbarkBuilderModuleWindowPrefabBase<Module, HorizontalScroller>
    {
        
        public override UIBreadcrumb GetBreadcrumb()
        {
            var crumb = new UIBreadcrumb
            {
                Id = "ImportCharacter",
                Title = "Choose Character",
                IconPath = UIBreadcrumb.DEFAULT_ICON,
                IconForegroundColor = ConsoleLib.Console.ColorUtility.ColorMap['y'],
                IconDetailColor = Color.clear,
            };

            if (module.data?.ID != null)
            {
                var aspect = module.Aspects.FirstOrDefault(x => x.ID == module.data.ID);
                if (aspect != null)
                {
                    crumb.Title = aspect.GetTitle();
                    crumb.IconPath = aspect.GetTile();
                    crumb.IconForegroundColor = aspect.GetForeground();
                    crumb.IconDetailColor = aspect.GetDetail();
                }
            }

            return crumb;
        }

        public override void RandomSelection()
        {
            prefabComponent.scrollContext
                .GetContextAt(XRL.Rules.Stat.Random(0, prefabComponent.choices.Count - 1))
                .Activate();
        }

        public IEnumerable<ChoiceWithColorIcon> GetSelections()
        {
            if (module.Aspects.Count == 0)
            {
                yield return new ChoiceWithColorIcon
                {
                    Id = "Empty",
                    Title = "Empty",
                    IconPath = "Mutations/temporal_fugue.bmp",
                    IconForegroundColor = ConsoleLib.Console.ColorUtility.ColorMap['k'],
                    IconDetailColor = ConsoleLib.Console.ColorUtility.ColorMap['k'],
                    Description = "There's nothing here.",
                    Chosen = x => false
                };
            }
            else
            {
                foreach (var aspect in module.Aspects)
                {
                    yield return new ChoiceWithColorIcon
                    {
                        Id = aspect.ID,
                        Title = aspect.GetTitle(),
                        IconPath = aspect.GetTile(),
                        IconForegroundColor = aspect.GetForeground(),
                        IconDetailColor = aspect.GetDetail(),
                        Description = aspect.GetDescription(),
                        Chosen = x => x.Id == module.Aspect?.ID
                    };
                }
            }
        }

        public void OnSelectFile(FrameworkDataElement choice)
        {
            if (choice.Id == "Empty") module.builder.back();
            else module.SelectAspect(choice.Id);
        }

        public override void BeforeShow(EmbarkBuilderModuleWindowDescriptor descriptor)
        {
            prefabComponent.autoHotkey = true;
            prefabComponent.onSelected.RemoveAllListeners();
            prefabComponent.onSelected.AddListener(OnSelectFile);
            prefabComponent.BeforeShow(descriptor, GetSelections());
            base.BeforeShow(descriptor);
        }

        public override void Show()
        {
            base.Show();

            if (!module.Aspects.Any())
            {
                Popup.NewPopupMessageAsync("There are no characters available for import.");
                GameManager.Instance.uiQueue.queueTask(module.builder.back);
            }
        }
    }
}
