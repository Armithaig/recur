﻿using System;
using System.Diagnostics;
using XRL.World;
using XRL.World.Parts;
using XRL.Wish;
using XRL.CharacterBuilds.Qud.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleLib.Console;
using Newtonsoft.Json;
using Recur.XML;
using XRL;
using XRL.CharacterBuilds;
using XRL.CharacterBuilds.Qud;
using XRL.Language;
using XRL.Messages;
using XRL.UI.Framework;
using XRL.World.Effects;
using static Recur.Static;

namespace Recur.UI
{
    public class Module : EmbarkBuilderModule<Module.Data>
    {
        [Serializable]
        [JsonObject(MemberSerialization.Fields)]
        public class Data : AbstractEmbarkBuilderModuleData
        {
            public string ID;

            public Data()
            {
            }

            public Data(string ID)
            {
                this.ID = ID;
            }
        }

        private List<Aspect> _Aspects;
        public List<Aspect> Aspects
        {
            get
            {
                if (_Aspects == null)
                {
                    _Aspects = Importer.ReadMetaData();
                    _Aspects.Sort();
                }

                return _Aspects;
            }
            set
            {
                _Aspects = value;
                Aspect = null;
            }
        }

        public Aspect Aspect;

        public Module()
        {
        }

        public override bool shouldBeEnabled() => builder?.GetModule<QudChartypeModule>()?.data?.type == "RecurImport";

        public override bool shouldBeEditable() => true; // HMMM

        public override void OnAfterDataChange(AbstractEmbarkBuilderModuleData OldValues, AbstractEmbarkBuilderModuleData NewValues)
        {
            var id = data?.ID;
            if (id != null && (Aspect == null || Aspect.ID != id))
            {
                Aspect = Aspects.FirstOrDefault(x => x.ID == id);
            }

            base.OnAfterDataChange(OldValues, NewValues);
        }

        public void SelectAspect(string ID)
        {
            setData(new Data(ID));
            if (Aspect == null || !Importer.Import(Aspect)) return;
            
            var player = Aspect.Player;
            var gtm = builder.GetModule<QudGenotypeModule>();
            if (gtm != null)
            {
                var genotype = player.GetProperty("Genotype", "");// ?? GenotypeFactory.Genotypes.GetRandomElement().Name;
                gtm.setData(new QudGenotypeModuleData(genotype));
            }
                
            var stm = builder.GetModule<QudSubtypeModule>();
            if (stm != null)
            {
                var subtype = player.GetProperty("Subtype", "");// ?? SubtypeFactory.Subtypes.GetRandomElement().Name;
                stm.setData(new QudSubtypeModuleData(subtype));
            }

            var abm = builder.GetModule<QudAttributesModule>();
            if (abm != null)
            {
                var attributes = new QudAttributesModuleData();
                var intrinsic = new Dictionary<string, int>();
                var gts = gtm?.data?.Entry?.Stats ?? GenotypeFactory.RequireGenotypeEntry("Mutated Human").Stats;
                var sts = stm?.data?.Entry?.Stats ?? new Dictionary<string, SubtypeStat>();
                foreach (var pair in gts)
                {
                    if (!intrinsic.ContainsKey(pair.Key)) intrinsic.Add(pair.Key, 0);
                    if (pair.Value.Minimum != Genotype.NO_VALUE) intrinsic[pair.Key] = pair.Value.Minimum;
                }

                foreach (var pair in sts)
                {
                    if (!intrinsic.ContainsKey(pair.Key)) intrinsic.Add(pair.Key, 0);
                    if (pair.Value.Minimum != Subtype.NO_VALUE) intrinsic[pair.Key] = pair.Value.Minimum;
                }

                foreach (var pair in gts)
                {
                    if (!intrinsic.ContainsKey(pair.Key)) intrinsic.Add(pair.Key, 0);
                    if (pair.Value.Bonus != Genotype.NO_VALUE) intrinsic[pair.Key] += pair.Value.Bonus;
                }

                foreach (var pair in sts)
                {
                    if (!intrinsic.ContainsKey(pair.Key)) intrinsic.Add(pair.Key, 0);
                    if (pair.Value.Bonus != Subtype.NO_VALUE) intrinsic[pair.Key] += pair.Value.Bonus;
                }
                    
                foreach (var stat in player.Statistics)
                {
                    if (Int32.TryParse(stat.Value, out var value))
                    {
                        if (Int32.TryParse(stat.Penalty, out var penalty)) value -= penalty;
                        if (intrinsic.TryGetValue(stat.Name, out var min)) value -= min;
                        else continue;
                        attributes.PointsPurchased[stat.Name] = value;
                    }
                }
                    
                abm.setData(attributes);
            }

            var mtm = builder.GetModule<QudMutationsModule>();
            if (mtm != null)
            {
                var mutations = new QudMutationsModuleData { mp = 0 };
                foreach (var part in player.Parts)
                {
                    if (part is XMLMutation mutation)
                    {
                        var entry = MutationFactory.GetMutationEntryByName(mutation.Name);
                        if (entry == null) continue;

#if BUILD_2_0_204
                        var variant = 0;
                        if (!mutation.Variant.IsNullOrEmpty())
                        {
                            variant = entry.CreateInstance().GetVariants().IndexOf(mutation.Variant);
                        }
#else
                        var variant = mutation.Variant;
#endif
                            
                        mutations.selections.Add(new QudMutationModuleDataRow
                        {
                            Mutation = entry.DisplayName,
                            Count = mutation.Level,
                            Variant = variant
                        });
                    }
                }
                    
                if (mutations.selections.Count > 0)
                {
                    mtm.setData(mutations);
                }
            }

            var cbm = builder.GetModule<QudCyberneticsModule>();
            if (cbm != null)
            {
                var cybernetics = new QudCyberneticsModuleData { lp = 0 };
                foreach (var part in player.Body.YieldParts())
                {
                    if (part.Implant == null) continue;
                    cybernetics.selections.Add(new QudCyberneticsModuleDataRow
                    {
                        Cybernetic = part.Implant.Blueprint,
                        Count = 1,
                        Variant = part.Type
                    });
                }

                if (cybernetics.selections.Count > 0)
                {
                    cbm.setData(cybernetics);
                }
            }

            var ccm = builder.GetModule<QudCustomizeCharacterModule>();
            if (ccm != null)
            {
                var customize = new QudCustomizeCharacterModuleData { name = player.GetDisplayName() };
                foreach (var pronominal in player.Pronominals)
                {
                    if (pronominal is XMLGender gender)
                    {
                        if (Gender.Genders.TryGetValue(gender.Name, out var value))
                        {
                            customize.gender = value;
                        }
                    }
                    else if (pronominal is XMLPronounSet set)
                    {
                        if (PronounSet.PronounSets.TryGetValue(set.Name, out var value))
                        {
                            customize.pronounSet = value;
                        }
                    }
                }
                    
                ccm.setData(customize);
            }

            try
            {
                builder.advanceToViewId("Chargen/BuildSummary");
            }
            catch (Exception ex)
            {
                RLog("Error advancing to build summary, skipping to customize", ex);
                builder.advanceToViewId("Chargen/Customize");
            }
        }

        public override void assembleWindowDescriptors(List<EmbarkBuilderModuleWindowDescriptor> Windows)
        {
            var i = Windows.FindIndex(x => x.viewID == "Chargen/CharType");
            Windows.Insert(i + 1, windows["Recur/Import"]);
        }

        public override SummaryBlockData GetSummaryBlock()
        {
            if (Aspect == null) return null;
            if (Aspect.Companions.IsNullOrEmpty()) return null;

            var block = new SummaryBlockData
            {
                Id = GetType().FullName,
                Title = "Companions",
                Description = String.Join("\n", Aspect.Companions.Select(x => x.GetDisplayName())),
                SortOrder = 200
            };

            return block;
        }

        public override void InitFromSeed(string seed)
        {
        }

        public override void OnEnabled()
        {
            Aspects = null;
        }

        public override void OnDisabled()
        {
            Aspects = null;
        }

        public override object handleUIEvent(string id, object element)
        {
            if (id == EmbarkBuilder.EventNames.EditableGameModeQuery)
            {
               return false;
            }
            
            return base.handleUIEvent(id, element);
        }

        public override object handleBootEvent(string ID, XRLGame Game, EmbarkInfo Info, object Element = null)
        {
            if (Aspect == null) return base.handleBootEvent(ID, Game, Info, Element);

            if (ID == QudGameBootModule.BOOTEVENT_AFTERBOOTPLAYEROBJECT)
            {
                Aspect.Player.Apply((GameObject) Element);
            }
            else if (ID == QudGameBootModule.BOOTEVENT_GAMESTARTING)
            {
                Aspect.Player.ApplyItems(The.Player);
                
                if (ImportReputation)
                {
                    try { Aspect.Reputation.Apply(); }
                    catch (Exception e) { RLog("Error loading reputations", e); }
                }

                if (ImportTinker)
                {
                    try { Aspect.Tinker.Apply(); }
                    catch (Exception e) { RLog("Error loading tinker data", e); }
                }

                if (ImportJournal)
                {
                    try { Aspect.Journal.Apply(); }
                    catch (Exception e) { RLog("Error loading journal", e); }
                }
                
                SpawnCompanions();
            }
            else if (ID == QudGameBootModule.BOOTEVENT_BOOTPLAYERTILE)
            {
                if (!string.IsNullOrEmpty(Aspect.Render?.Tile))
                {
                    return Aspect.Render.Tile;
                }
            }
            else if (ID == QudGameBootModule.BOOTEVENT_BOOTPLAYEROBJECTBLUEPRINT)
            {
                return Aspect.Player?.Blueprint ?? Element;
            }
            else if (ID == QudGameBootModule.BOOTEVENT_BOOTPLAYERTILEFOREGROUND)
            {
                var fg = Aspect.Render?.GetForeground();
                if (fg != null)
                {
                    return fg.Value.ToString();
                }
            }
            else if (ID == QudGameBootModule.BOOTEVENT_BOOTPLAYERTILEDETAIL)
            {
                if (!string.IsNullOrEmpty(Aspect.Render?.DetailColor))
                {
                    return Aspect.Render.DetailColor;
                }
            }

            return base.handleBootEvent(ID, Game, Info, Element);
        }

        public void SpawnCompanions()
        {
            if (!ImportFollower) return;
            foreach (var companion in Aspect.Companions)
            {
                try
                {
                    var cell = The.Player.CurrentCell;
                    var obj = companion.Create();
                    var target = cell.GetRandomEmptyLocalAdjacentCell(2);
                    var temp = The.ZoneManager.GetZone(cell.ParentZone.GetZoneWorld()) ?? cell.ParentZone;
                    // Build up out of sight on the world map, hehehe.
                    temp.GetCell(0, 0).AddObject(obj);
                    companion.ApplyItems(obj);

                    MessageQueue.Suppress = true;
                    if (companion.Source == "Beguile")
                        obj.ApplyEffect(new Beguiled(The.Player));
                    else if (companion.Source == "Proselytize")
                        obj.ApplyEffect(new Proselytized(The.Player));
                    else
                        obj.pBrain.BecomeCompanionOf(The.Player);
                    MessageQueue.Suppress = false;

                    obj.SystemMoveTo(target, energyCost: 0, forced: true);

                    // Added late so missed the zone activation.
                    obj.MakeActive();
                }
                catch (Exception e)
                {
                    RLog("Error creating companion " + companion.Blueprint, e);
                }
            }
        }
    }
}
